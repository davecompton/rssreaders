# rssreaders
| Reader       | OPML import/export | folders? | $/Year | opensource |   api|  browser | ios  | android | notes
|--------------|-------------------:|---------:|-------:|-----------:|-----:|---------:|-----:|--------:|-------:|
| inoreader    | yes                | yes      |$20     | no         |   yes|       yes| ?    |      yes| 
| miniflux     | yes                |       yes|$15     | yes        |   yes|       yes| ?    |       no| updates unpredictably
| feedbin      | yes                |       yes|$60     | yes        |   yes|       yes|   pwa|      pwa| updates faster than inoreader